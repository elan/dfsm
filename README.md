# DFSM - Le Dictionnaire de Français Scientifique Médiéval.

## Installation
```
make init
### définir les ports apache et adminer ainsi que le mot de passe root mysql, un nom de db, un utilisateur et mot de passe mysql
### reporter ces valeurs dans le `.env` de Symfony.
```
L'application devrait être dispo sur `localhost:XXX` où `XXX` est le port défini pour apache.

## Mise à jour
```
docker-compose exec apache make update
```

## Licence

GNU GENERAL PUBLIC LICENSE V3 (voir le [Guide rapide de la GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html))
