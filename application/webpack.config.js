var Encore = require('@symfony/webpack-encore');

Encore
  .setOutputPath('public/build/')
  .setPublicPath('/build')
  .addEntry('js/main', './assets/js/main.js')
  .addEntry('js/edit-sense', './assets/js/edit-sense.js')
  .addEntry('js/search', './assets/js/search.js')
  .addEntry('js/prototype', './assets/js/prototype.js')
  .addEntry('js/filterEntry', './assets/js/filterEntry.js')
  .addStyleEntry('css/template', './assets/css/template.css')
  .addStyleEntry('css/project', './assets/css/project.css')
  .addStyleEntry('css/toastr', './node_modules/toastr/build/toastr.min.css')
  .enableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning()
  .createSharedEntry('js/vendor', './assets/js/vendor.js')
  .autoProvidejQuery()
  .autoProvideVariables({
    Toastr: 'toastr',
    bsCustomFileInput: 'bs-custom-file-input'
  });

module.exports = Encore.getWebpackConfig();