<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EtymRepository")
 */
class Etym
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $src;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mentioned;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Entry", mappedBy="etym", cascade={"persist", "remove"})
     */
    private $entry;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSrc(): ?string
    {
        return $this->src;
    }

    public function setSrc(?string $src): self
    {
        $this->src = $src;

        return $this;
    }

    public function getMentioned(): ?string
    {
        return $this->mentioned;
    }

    public function setMentioned(?string $mentioned): self
    {
        $this->mentioned = $mentioned;

        return $this;
    }

    public function getEntry(): ?Entry
    {
        return $this->entry;
    }

    public function setEntry(?Entry $entry): self
    {
        $this->entry = $entry;

        // set (or unset) the owning side of the relation if necessary
        $newEtym = null === $entry ? null : $this;
        if ($entry->getEtym() !== $newEtym) {
            $entry->setEtym($newEtym);
        }

        return $this;
    }
}
