<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DomainRepository")
 */
class Domain
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medValue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $modValue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medValueShort;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $modValueShort;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sense", mappedBy="domain")
     */
    private $senses;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMedValue(): ?string
    {
        return $this->medValue;
    }

    public function setMedValue(?string $medValue): self
    {
        $this->medValue = $medValue;

        return $this;
    }

    public function getModValue(): ?string
    {
        return $this->modValue;
    }

    public function setModValue(?string $modValue): self
    {
        $this->modValue = $modValue;

        return $this;
    }

    public function getMedValueShort(): ?string
    {
        return $this->medValueShort;
    }

    public function setMedValueShort(?string $medValueShort): self
    {
        $this->medValueShort = $medValueShort;

        return $this;
    }

    public function getModValueShort(): ?string
    {
        return $this->modValueShort;
    }

    public function setModValueShort(?string $modValueShort): self
    {
        $this->modValueShort = $modValueShort;

        return $this;
    }
}
