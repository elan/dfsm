<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NoteRepository")
 */
class Note
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Xr", mappedBy="note", cascade={"persist", "remove"})
     */
    private $xrs;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sense", inversedBy="notes")
     */
    private $sense;

    public function __construct()
    {
        $this->xrs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Xr[]
     */
    public function getXrs(): Collection
    {
        return $this->xrs;
    }

    public function addXr(Xr $xr): self
    {
        if (!$this->xrs->contains($xr)) {
            $this->xrs[] = $xr;
            $xr->setNote($this);
        }

        return $this;
    }

    public function removeXr(Xr $xr): self
    {
        if ($this->xrs->contains($xr)) {
            $this->xrs->removeElement($xr);
            // set the owning side to null (unless already changed)
            if ($xr->getNote() === $this) {
                $xr->setNote(null);
            }
        }

        return $this;
    }

    public function getSense(): ?Sense
    {
        return $this->sense;
    }

    public function setSense(?Sense $sense): self
    {
        $this->sense = $sense;

        return $this;
    }
}
