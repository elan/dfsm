<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\XrRepository")
 */
class Xr
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Note", inversedBy="xrs")
     */
    private $note;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ref;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $gloss;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entry", inversedBy="xrs")
     */
    private $entry;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNote(): ?Note
    {
        return $this->note;
    }

    public function setNote(?Note $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(?string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getGloss(): ?string
    {
        return $this->gloss;
    }

    public function setGloss(?string $gloss): self
    {
        $this->gloss = $gloss;

        return $this;
    }

    public function getEntry(): ?Entry
    {
        return $this->entry;
    }

    public function setEntry(?Entry $entry): self
    {
        $this->entry = $entry;

        return $this;
    }
}
