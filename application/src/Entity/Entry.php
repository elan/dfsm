<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EntryRepository")
 * @ORM\Table(indexes={
 *  @ORM\Index(name="lemma", columns={"lemma"}),
 *  @ORM\Index(name="cleanlemma", columns={"clean_lemma"}),
 *  @ORM\Index(name="initial", columns={"initial"})
 * })
 */
class Entry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $xmlContent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lemma;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $initial;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cleanLemma;

    /**
     * @ORM\Column(type="boolean")
     */
    private $valid = true;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $previousXmlContent;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Gram", cascade={"persist"})
     */
    private $grams;

    /**
     * @ORM\Column(type="date")
     */
    private $dateCreation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $idDfsm;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Etym", inversedBy="entry", cascade={"persist", "remove"})
     */
    private $etym;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sense", mappedBy="entry", cascade={"persist", "remove"})
     */
    private $senses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Xr", mappedBy="entry", cascade={"persist", "remove"})
     */
    private $xrs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Log", mappedBy="entry", cascade={"remove"})
     */
    private $logs;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEdition;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $auteur;

    public function __construct()
    {
        $this->grams = new ArrayCollection();
        $this->senses = new ArrayCollection();
        $this->xrs = new ArrayCollection();
        $this->logs = new ArrayCollection();
    }

    public function isVariante(): ?bool 
    {
        if (count($this->senses) == 0 && count($this->xrs) == 1) {
            return true;
        }

        return false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getXmlContent(): ?string
    {
        return $this->xmlContent;
    }

    public function setXmlContent(?string $xmlContent): self
    {
        $this->xmlContent = $xmlContent;

        return $this;
    }

    public function getLemma(): ?string
    {
        return $this->lemma;
    }

    public function setLemma(string $lemma): self
    {
        $this->lemma = $lemma;

        return $this;
    }

    public function getInitial(): ?string
    {
        return $this->initial;
    }

    public function setInitial(string $initial): self
    {
        $this->initial = $initial;

        return $this;
    }

    public function getCleanLemma(): ?string
    {
        return $this->cleanLemma;
    }

    public function setCleanLemma(string $cleanLemma): self
    {
        $this->cleanLemma = $cleanLemma;

        return $this;
    }

    public function getValid(): ?bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): self
    {
        $this->valid = $valid;

        return $this;
    }

    public function getPreviousXmlContent(): ?string
    {
        return $this->previousXmlContent;
    }

    public function setPreviousXmlContent(?string $previousXmlContent): self
    {
        $this->previousXmlContent = $previousXmlContent;

        return $this;
    }

    /**
     * @return Collection|Gram[]
     */
    public function getGrams(): Collection
    {
        return $this->grams;
    }

    public function addGram(Gram $gram): self
    {
        if (!$this->grams->contains($gram)) {
            $this->grams[] = $gram;
        }

        return $this;
    }

    public function removeGram(Gram $gram): self
    {
        if ($this->grams->contains($gram)) {
            $this->grams->removeElement($gram);
        }

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getIdDfsm(): ?string
    {
        return $this->idDfsm;
    }

    public function setIdDfsm(?string $idDfsm): self
    {
        $this->idDfsm = $idDfsm;

        return $this;
    }

    public function getEtym(): ?Etym
    {
        return $this->etym;
    }

    public function setEtym(?Etym $etym): self
    {
        $this->etym = $etym;

        return $this;
    }

    /**
     * @return Collection|Sense[]
     */
    public function getSenses(): Collection
    {
        return $this->senses;
    }

    public function addSense(Sense $sense): self
    {
        if (!$this->senses->contains($sense)) {
            $this->senses[] = $sense;
            $sense->setEntry($this);
        }

        return $this;
    }

    public function removeSense(Sense $sense): self
    {
        if ($this->senses->contains($sense)) {
            $this->senses->removeElement($sense);
            // set the owning side to null (unless already changed)
            if ($sense->getEntry() === $this) {
                $sense->setEntry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Xr[]
     */
    public function getXrs(): Collection
    {
        return $this->xrs;
    }

    public function addXr(Xr $xr): self
    {
        if (!$this->xrs->contains($xr)) {
            $this->xrs[] = $xr;
            $xr->setEntry($this);
        }

        return $this;
    }

    public function removeXr(Xr $xr): self
    {
        if ($this->xrs->contains($xr)) {
            $this->xrs->removeElement($xr);
            // set the owning side to null (unless already changed)
            if ($xr->getEntry() === $this) {
                $xr->setEntry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Log[]
     */
    public function getLogs(): Collection
    {
        return $this->logs;
    }

    public function addLog(Log $log): self
    {
        if (!$this->logs->contains($log)) {
            $this->logs[] = $log;
            $log->setEntry($this);
        }

        return $this;
    }

    public function removeLog(Log $log): self
    {
        if ($this->logs->contains($log)) {
            $this->logs->removeElement($log);
            // set the owning side to null (unless already changed)
            if ($log->getEntry() === $this) {
                $log->setEntry(null);
            }
        }

        return $this;
    }

    public function getDateEdition(): ?\DateTimeInterface
    {
        return $this->dateEdition;
    }

    public function setDateEdition(\DateTimeInterface $dateEdition): self
    {
        $this->dateEdition = $dateEdition;

        return $this;
    }

    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(?string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }
}
