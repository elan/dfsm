<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CitRepository")
 */
class Cit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $quote;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sense", inversedBy="cits")
     */
    private $sense;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $biblText;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $chronologicalIndex = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuote(): ?string
    {
        return $this->quote;
    }

    public function setQuote(string $quote): self
    {
        $this->quote = $quote;

        return $this;
    }

    public function getSense(): ?Sense
    {
        return $this->sense;
    }

    public function setSense(?Sense $sense): self
    {
        $this->sense = $sense;

        return $this;
    }

    public function getBiblText(): ?string
    {
        return $this->biblText;
    }

    public function setBiblText(?string $biblText): self
    {
        $this->biblText = $biblText;

        return $this;
    }

    public function getChronologicalIndex(): ?int
    {
        return $this->chronologicalIndex;
    }

    public function setChronologicalIndex(?int $chronologicalIndex): self
    {
        $this->chronologicalIndex = $chronologicalIndex;

        return $this;
    }
}
