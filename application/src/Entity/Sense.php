<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SenseRepository")
 */
class Sense
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $def;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entry", inversedBy="senses")
     */
    private $entry;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $usg;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cit", mappedBy="sense", cascade={"persist", "remove"})
     * @ORM\OrderBy({"chronologicalIndex"="ASC"})
     */
    private $cits;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Note", mappedBy="sense", cascade={"persist", "remove"})
     */
    private $notes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain", inversedBy="senses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $domain;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imgPath;

    public function __construct()
    {
        $this->cits = new ArrayCollection();
        $this->notes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDef(): ?string
    {
        return $this->def;
    }

    public function setDef(?string $def): self
    {
        $this->def = $def;

        return $this;
    }

    public function getUsg(): ?string
    {
        return $this->usg;
    }

    public function setUsg(?string $usg): self
    {
        $this->usg = $usg;

        return $this;
    }

    /**
     * @return Collection|Cit[]
     */
    public function getCits(): Collection
    {
        return $this->cits;
    }

    public function addCit(Cit $cit): self
    {
        if (!$this->cits->contains($cit)) {
            $this->cits[] = $cit;
            $cit->setSense($this);
        }

        return $this;
    }

    public function removeCit(Cit $cit): self
    {
        if ($this->cits->contains($cit)) {
            $this->cits->removeElement($cit);
            // set the owning side to null (unless already changed)
            if ($cit->getSense() === $this) {
                $cit->setSense(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setSense($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->contains($note)) {
            $this->notes->removeElement($note);
            // set the owning side to null (unless already changed)
            if ($note->getSense() === $this) {
                $note->setSense(null);
            }
        }

        return $this;
    }

    public function getEntry(): ?Entry
    {
        return $this->entry;
    }

    public function setEntry(?Entry $entry): self
    {
        $this->entry = $entry;

        return $this;
    }

    public function getDomain(): ?Domain
    {
        return $this->domain;
    }

    public function setDomain(?Domain $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getImgPath(): ?string
    {
        return $this->imgPath;
    }

    public function setImgPath(?string $imgPath): self
    {
        $this->imgPath = $imgPath;

        return $this;
    }
}
