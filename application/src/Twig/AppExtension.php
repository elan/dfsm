<?php

namespace App\Twig;

use App\Entity\Domain;
use App\Entity\Entry;
use App\Entity\Gram;
use App\Entity\Log;
use App\Entity\Sense;
use App\Manager\EntryManager;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    private $entryManager;
    private $em;


    public function __construct(EntryManager $entryManager, EntityManagerInterface $em)
    {
        $this->entryManager = $entryManager;
        $this->em = $em;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getSearchForm', [$this, 'getSearchForm']),
            new TwigFunction('getAdvancedSearchForm', [$this, 'getAdvancedSearchForm']),
            new TwigFunction('getDomainsMed', [$this, 'getDomainsMed']),
            new TwigFunction('getModsFromDomainMed', [$this, 'getModsFromDomainMed']),
            new TwigFunction('getXrLink', [$this, 'getXrLink']),
            new TwigFunction('getInitials', [$this, 'getInitials']),
            new TwigFunction('getEntriesCount', [$this, 'getEntriesCount']),
            new TwigFunction('getSensesCountByDom', [$this, 'getSensesCountByDom']),
            new TwigFunction('sensesSorted', [$this, 'sensesSorted']),
            new TwigFunction('getEntryLogsGrouped', [$this, 'getByEntryGrouped'])
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('cleanExport', [$this, 'cleanExport']),
        ];
    }

    public function cleanExport($str)
    {
        $str = preg_replace("/<nonit>(.+?)<\/nonit>/is", "<hi rend='nonit'>$1</hi>", $str);
        $str = preg_replace("/<source>(.+?)<\/source>/is", "<title>$1</title>", $str);
        $str = preg_replace("/<mark>(.+?)<\/mark>/is", "<hi rend='mark'>$1</hi>", $str);
        $str = preg_replace("/<fleur>(.+?)<\/fleur>/is", "<hi rend='fleur'>$1</hi>", $str);
        $str = preg_replace("/<em.*?>(.+?)<\/em>/is", "<hi rend='italic'>$1</hi>", $str);
        $str = preg_replace("/style=\".*\"/is", "", $str);
        $str = preg_replace("/<p.*?>(.+?)<\/p>/is", "$1", $str);

        return $str;
    }

    public function getByEntryGrouped(Entry $entry)
    {
        return $this->em->getRepository(Log::class)->getByEntryGrouped($entry);
    }
    
    public function sensesSorted(Entry $entry)
    {
        return $this->em->getRepository(Sense::class)->sortByDomain($entry);
    }

    public function getEntriesCount(Gram $gram)
    {
        return count($this->em->getRepository(Entry::class)->findByGramGrp($gram));
    }

    public function getSensesCountByDom(Domain $domain)
    {
        return count($this->em->getRepository(Sense::class)->findByDomain($domain));
    }

    public function getSearchForm()
    {
        return $this->entryManager->getSearchForm();
    }

    public function getAdvancedSearchForm()
    {
        return $this->entryManager->getAdvancedSearchForm();
    }

    public function getInitials()
    {
        return $this->em->getRepository(Entry::class)->getDistinctInitials();
    }

    public function getDomainsMed()
    {
        return $this->em->getRepository(Domain::class)->getDistinctMedValues();
    }

    public function getModsFromDomainMed($domainMed)
    {
        return $this->em->getRepository(Domain::class)->findByMedAndValidSorted($domainMed);
    }

    public function getXrLink($lemma)
    {
        if ($entry = $this->em->getRepository(Entry::class)->findOneBy(["lemma" => $lemma, ])) {
            return $entry->getId();
        }

        return null;
    }
}
