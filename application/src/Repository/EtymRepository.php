<?php

namespace App\Repository;

use App\Entity\Etym;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Etym|null find($id, $lockMode = null, $lockVersion = null)
 * @method Etym|null findOneBy(array $criteria, array $orderBy = null)
 * @method Etym[]    findAll()
 * @method Etym[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EtymRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Etym::class);
    }

    // /**
    //  * @return Etym[] Returns an array of Etym objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Etym
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
