<?php

namespace App\Repository;

use App\Entity\Cit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Cit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cit[]    findAll()
 * @method Cit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cit::class);
    }

    public function findAllByPartialBiblText($needle)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.biblText LIKE :needle')
            ->setParameter('needle', "%".$needle."%")
            ->getQuery()
            ->getResult()
        ;
    }
}
