<?php

namespace App\Repository;

use App\Entity\Gram;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Gram|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gram|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gram[]    findAll()
 * @method Gram[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GramRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gram::class);
    }

    // /**
    //  * @return Gram[] Returns an array of Gram objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Gram
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
