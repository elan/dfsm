<?php

namespace App\Repository;

use App\Entity\Sense;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Sense|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sense|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sense[]    findAll()
 * @method Sense[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SenseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sense::class);
    }


    public function findByDomainSorted($domain)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.domain = :domain')
            ->join('s.entry', 'e')
            ->orderBy('e.lemma', 'ASC')
            ->setParameter('domain', $domain)
            ->getQuery()
            ->getResult()
        ;
    }

    public function sortByDomain($entry)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.entry = :entry')
            ->join('s.domain', 'd')
            ->addOrderBy('d.medValueShort', 'ASC')
            ->addOrderBy('d.modValueShort', 'ASC')
            ->setParameter('entry', $entry)
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Sense
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
