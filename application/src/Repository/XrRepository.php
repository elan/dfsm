<?php

namespace App\Repository;

use App\Entity\Xr;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Xr|null find($id, $lockMode = null, $lockVersion = null)
 * @method Xr|null findOneBy(array $criteria, array $orderBy = null)
 * @method Xr[]    findAll()
 * @method Xr[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class XrRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Xr::class);
    }
    
    public function findBadOnesforNotes()
    {
        return $this->createQueryBuilder('x')
            ->leftJoin("x.note", "note")
            ->andWhere('note.id IS NOT NULL')
            ->andWhere('x.type NOT IN (:types)')
            ->setParameter('types', ["var", "syn", "encyclo", "ant", "hyp", "nomen", "cohyp", "gloss", "V."])
            ->orderBy('x.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findDistinctforNotes()
    {
        return $this->createQueryBuilder('x')
            ->select("x.type")
            ->leftJoin("x.note", "note")
            ->andWhere('note.id IS NOT NULL')
            ->orderBy('x.id', 'ASC')
            ->groupBy("x.type")
            ->getQuery()
            ->getResult()
        ;
    }

    public function findDistinctforEntries()
    {
        return $this->createQueryBuilder('x')
            ->select("x.type")
            ->leftJoin("x.entry", "entry")
            ->andWhere('entry.id IS NOT NULL')
            ->orderBy('x.id', 'ASC')
            ->groupBy("x.type")
            ->getQuery()
            ->getResult()
        ;
    }

    public function findBadOnesforEntries()
    {
        return $this->createQueryBuilder('x')
            ->leftJoin("x.entry", "entry")
            ->andWhere('entry.id IS NOT NULL')
            ->andWhere('x.type NOT IN (:types)')
            ->setParameter('types', ["var", "syn", "encyclo", "ant", "hyp", "nomen", "cohyp", "gloss", "V."])
            ->orderBy('x.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
