<?php

namespace App\Repository;

use App\Entity\Domain;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Domain|null find($id, $lockMode = null, $lockVersion = null)
 * @method Domain|null findOneBy(array $criteria, array $orderBy = null)
 * @method Domain[]    findAll()
 * @method Domain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DomainRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Domain::class);
    }

    public function findByMedAndValidSorted($domainMed)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.medValueShort = :medValueShort')
            ->join('d.senses', 's')
            ->join('s.entry', 'e')
            ->andWhere('e.valid = :valid')
            ->orderBy('d.modValueShort', 'ASC')
            ->setParameter('medValueShort', $domainMed["medValueShort"])
            ->setParameter('valid', true)
            ->distinct()
            ->getQuery()
            ->getResult()
        ;
    }

    public function getDistinctMedValues()
    {
        return $this->createQueryBuilder('d')
            ->select('d.medValueShort, d.medValue')
            ->join('d.senses', 's')
            ->join('s.entry', 'e')
            ->andWhere('e.valid = true')
            ->orderBy('d.medValueShort', 'ASC')
            ->distinct()
            ->getQuery()
            ->getResult()
        ;
    }
}
