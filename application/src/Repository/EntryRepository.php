<?php

namespace App\Repository;

use App\Entity\Entry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Entry|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entry|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entry[]    findAll()
 * @method Entry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entry::class);
    }

    public function getDistinctInitials()
    {
        return $this->createQueryBuilder('e')
          ->select('e.initial')
          ->andWhere('e.valid = :valid')
          ->orderBy('e.initial', 'ASC')
          ->distinct()
          ->setParameter('valid', true)
          ->getQuery()
          ->getResult()
      ;
    }

    public function findByGramGrp($gram)
    {
        $query = $this->createQueryBuilder('e')
        ->andWhere(":gram MEMBER OF e.grams")
        ->orderBy('e.lemma', 'ASC')
        ->setParameter('gram', $gram)
        ->getQuery();

        return $query->getResult();
    }


    public function findByDomainMed($medValueShort)
    {
        $query = $this->createQueryBuilder('e')
        ->join('e.senses', 's')
        ->join('s.domain', 'd')
        ->andWhere('e.valid = :valid')
        ->andWhere('d.medValueShort = :domain')
        ->orderBy('e.lemma', 'ASC')
        ->setParameter('domain', $medValueShort)
        ->setParameter('valid', true)
        ->getQuery();

        return $query->getResult();
    }

    public function findByDomain($domain)
    {
        $query = $this->createQueryBuilder('e')
        ->join('e.senses', 's')
        ->andWhere('e.valid = :valid')
        ->andWhere('s.domain = :domain')
        ->orderBy('e.lemma', 'ASC')
        ->setParameter('domain', $domain)
        ->setParameter('valid', true)
        ->getQuery();

        return $query->getResult();
    }

    public function findByPartialLemma($stringSearch, $stringCleaned)
    {
        $query = $this->createQueryBuilder('e')
        ->andWhere('e.valid = :valid')
        ->andWhere('e.lemma LIKE :stringSearch OR e.cleanLemma LIKE :stringCleaned')
        ->orderBy('e.lemma', 'ASC')
        ->setParameter('stringCleaned', '%'.$stringCleaned.'%')
        ->setParameter('stringSearch', '%'.$stringSearch.'%')
        ->setParameter('valid', true)
        ->getQuery();

        return $query->getResult();
    }

    public function findByStartWith($start, $cleanStart)
    {
        $query = $this->createQueryBuilder('e')
          ->andWhere('e.valid = :valid')
          ->andWhere('e.lemma LIKE :stringSearch OR e.cleanLemma LIKE :stringCleaned')
          ->orderBy('e.lemma', 'ASC')
          ->setParameter('stringCleaned', $cleanStart.'%')
          ->setParameter('stringSearch', $start.'%')
          ->setParameter('valid', true)
          ->getQuery();

        return $query->getResult();
    }

    public function findByAdvancedSearch($data)
    {
        $query = $this->createQueryBuilder('e')
          ->andWhere('e.valid = :valid')
          ->setParameter('valid', true)
          ->leftJoin('e.senses', 's')
          ->leftJoin('e.etym', 'eetym')
          ->leftJoin('s.notes', 'n')
          ->leftJoin('s.cits', 'c')
          ->leftJoin('e.xrs', 'exrs')
          ->leftJoin('n.xrs', 'nxrs')
          ->orderBy('e.lemma', 'ASC');

        if ($orth = $data["orth"]) {
            $query = $query
              ->andWhere('e.lemma LIKE :orth OR e.cleanLemma LIKE :orth')
              ->setParameter('orth', '%'.strtoupper($orth).'%');
        }
        if ($def = $data["def"]) {
            $query = $query
              ->andWhere('s.def LIKE :def')
              ->setParameter('def', '%'.$def.'%');
        }
        if ($usg = $data["usg"]) {
            $query = $query
              ->andWhere('s.usg LIKE :usg')
              ->setParameter('usg', '%'.$usg.'%');
        }
        if ($domain = $data["domain"]) {
            $query = $query
              ->andWhere('s.domain = :domain')
              ->setParameter('domain', $domain);
        }
        if ($etym = $data["etym"]) {
            $query = $query
              ->andWhere('eetym.mentioned LIKE :etym')
              ->setParameter('etym', '%'.$etym.'%');
        }
        if ($gloss = $data["gloss"]) {
            $query = $query
              ->andWhere('exrs.gloss LIKE :gloss OR nxrs.gloss LIKE :gloss')
              ->setParameter('gloss', '%'.$gloss.'%');
        }

        if ($quote = $data["quote"]) {
            $query = $query
              ->andWhere('c.quote LIKE :quote')
              ->setParameter('quote', '%'.$quote.'%');
        }
        if ($biblText = $data["biblText"]) {
            $query = $query
              ->andWhere('c.biblText LIKE :biblText')
              ->setParameter('biblText', '%'.$biblText.'%');
        }

        $query = $query->getQuery();

        return $query->getResult();
    }
}
