<?php

namespace App\Command;

use App\Entity\Cit;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class OnetimeCommand extends Command
{
    protected static $defaultName = 'app:onetime-command';

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('One time command to fix stuff')
            ->addOption('force', null, InputOption::VALUE_NONE, 'changes will be applied to the db')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $cits = $this->em->getRepository(Cit::class)->findAllByPartialBiblText("%chap. ap.%");
        $io->text(count($cits) ." entrée(s) concernée(s)");


        if ($input->getOption('force')) {
            foreach ($cits as $cit) {
                $io->text("✓ ".$cit->getSense()->getEntry()->getLemma());
                $text = $cit->getBiblText();
                $text = str_replace("chap. ap.", "chap.", $text);
                $cit->setBiblText($text);

                $this->em->persist($cit);
            }

            $this->em->flush();
        } else {
            $io->text("Rien n'a été fait; Utiliser l'option '--force' pour traiter les entrées");
        }

        $io->success('ok !');

        return 0;
    }
}
