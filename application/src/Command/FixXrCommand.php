<?php

namespace App\Command;

use App\Entity\Xr;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FixXrCommand extends Command
{
    private $em;
    protected static $defaultName = 'app:fix-xr';

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Change XR type')
            ->addArgument('from', InputArgument::REQUIRED, 'original value')
            ->addArgument('to', InputArgument::REQUIRED, 'target value')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $from = $input->getArgument('from');
        $to = $input->getArgument('to');

        if ($from && $to) {
            if ($xrs = $this->em->getRepository(Xr::class)->findByType($from)) {
                foreach ($xrs as $xr) {
                    $xr->setType($to);
                    $this->em->persist($xr);
                    $io->success($from . " -> " . $to);
                }
                $this->em->flush();
            } else {
                $io->warning('no xr found');
            }

        } else {
            $io->error('missing args');
        }
    }
}
