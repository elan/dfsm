<?php

namespace App\Command;

use App\Entity\Entry;
use App\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class DeleteAllEntriesCommand extends Command
{
    private $em;
    private $um;
    protected static $defaultName = 'app:deleteallentries';

    public function __construct(EntityManagerInterface $em, UserManager $um)
    {
        $this->um = $um;
        $this->em = $em;
        parent::__construct();
    }


    protected function configure()
    {
        $this->setDescription('Delete all entries');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Êtes-vous sûr de vouloir supprimer toutes les entrées ?', false);

        if (!$helper->ask($input, $output, $question)) {
            return;
        }

        $io->success("Suppression en cours...");
        $entries = $this->em->getRepository(Entry::class)->findAll();
        foreach ($entries as $entry) {
            $this->em->remove($entry);
        }
        $this->em->flush();
        $io->success("Suppression terminée");
    }
}
