<?php

namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;

class UserManager
{
    private $em;
    private $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function setRole(User $user, $roleToToggle)
    {
        $user->setRoles($roleToToggle);

        $this->saveUser($user);

        return;
    }

    public function create($mail, $password, $username, $passwordEncoder)
    {
        $user = new User();
        $user->setUsername($username);
        $user->setEmail($mail);
        $user->setLastAccess(new \DateTime());
        $user->setPassword($passwordEncoder->encodePassword($user, $password));

        $user = $this->saveUser($user);

        return $user;
    }

    public function getCurrentUser()
    {
        return $this->security->getUser();
    }

    public function anonymiseUsers()
    {
        $users = $this->em->getRepository(User::class)->getNonAnonymisedYet();
        foreach ($users as $user) {
            echo "Anonymisation userId: ".$user->getId()."\n";
            $this->anonymizeUserAccount($user, "full");
        }
        $this->em->flush();

        return;
    }

    public function anonymizeUserAccount(User $user)
    {
        $user->setEmail('anonymous-email-'.$user->getId().'@anonymous.fr');
        $user->setResetToken(null);
        $user->setLastAccess(null);
        $user->setAnonymous(true);
        $this->setRole($user, []);

        $this->saveUser($user);

        return;
    }

    public function saveUser(User $user)
    {
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }
}
