<?php

namespace App\Manager;

use App\Entity\Entry;
use App\Entity\Etym;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;

class EtymManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    public function getAndAdd(Entry $entry, $etymoSrc, $mentioned)
    {
        if ($etym = $entry->getEtym()) {
            $entry->setEtym(null);
            $this->em->remove($etym);
        }

        $etym = new Etym;
        $etym->setSrc($etymoSrc);
        $etym->setMentioned($mentioned);

        $entry->setEtym($etym);

        return $entry;
    }
}
