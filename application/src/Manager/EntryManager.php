<?php

namespace App\Manager;

use App\Entity\Entry;
use Twig\Environment;
use App\Form\SearchEntryType;
use App\Form\AdvancedSearchEntryType;
use App\Manager\EtymManager;
use App\Manager\GramManager;
use App\Manager\SenseManager;
use App\Manager\LogManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Dompdf\Dompdf;
use Dompdf\Options;

class EntryManager
{
    private $em;
    private $lm;
    private $formFactory;
    private $gramManager;
    private $etymManager;
    private $senseManager;
    private $params;
    private $twig;

    public function __construct(EntityManagerInterface $em, FormFactoryInterface $formFactory, GramManager $gramManager, EtymManager $etymManager, SenseManager $senseManager, LogManager $lm, ParameterBagInterface $params, Environment $twig)
    {
        $this->em = $em;
        $this->lm = $lm;
        $this->formFactory = $formFactory;
        $this->gramManager = $gramManager;
        $this->etymManager = $etymManager;
        $this->senseManager = $senseManager;
        $this->params = $params;
        $this->twig = $twig;
    }

    public function getSearchForm()
    {
        return $this->formFactory->create(SearchEntryType::class)->createView();
    }

    public function getAdvancedSearchForm()
    {
        return $this->formFactory->create(AdvancedSearchEntryType::class)->createView();
    }

    public function importEntry($content, $autoValid)
    {
        $xmlContent = iconv("UTF-8", "UTF-8//IGNORE", $content);

        $crawler = new Crawler();
        $crawler->addHTMLContent($xmlContent);

        $orth = $this->extractXML($crawler, "//form/orth");

        if (!$entry = $this->em->getRepository(Entry::class)->findOneByLemma($orth)) {
            $entry = new Entry();
            $entry->setDateCreation(new \DateTime());
        }

        $this->xml2entities($entry, $xmlContent);
        // $entry->setAuteur($auteur);
        $entry->setValid($autoValid);
        $entry->setInitial($this->getInitial($orth));
        $entry->setCleanLemma($this->removeDiacritic($orth));
        $entry->setLemma($orth);
        $entry->setIdDfsm($this->extractXML($crawler, "//id"));
        $entry->setDateEdition(new \DateTime());
        $entry->setXmlContent($xmlContent);
        $this->em->persist($entry);
        $this->em->flush($entry);

        $this->lm->createLog($entry, "Import fiche");

        $this->entities2xml($entry);
        return;
    }

    public function xml2entities(Entry $entry, $xmlContent)
    {
        $crawler = new Crawler();
        $crawler->addHTMLContent($xmlContent);

        $orth = $this->extractXML($crawler, "//form/orth");
        $dateDfsm = $this->extractXML($crawler, "//form/date");
        $valid = $this->extractXML($crawler, "//form/valid");

        $xrs = $crawler->filterXPath('//entry/xr')->each(function (Crawler $node) {
            $ref = $node->filterXPath("//ref")->text("");
            $gloss = $node->filterXPath("//gloss")->text("");
            $typeXR = $node->attr("type") ? $node->attr("type") : null;
            return ["type" => $typeXR, "ref" => $ref, "gloss" => $gloss];
        });
        $this->senseManager->handleXrs($entry, $xrs, true);

        // GRAM
        $grams = $crawler->filterXPath('//gram')->each(function (Crawler $node) {
            $type = $node->attr('type');
            $text = $node->text();

            return ["text" => $text, "type" => $type];
        });
        $this->gramManager->getAndAdd($entry, $grams);

        // ETYM
        $etymoSrc = $this->extractXML($crawler, "//etym/bibl/etymosrc");
        $mentioned = $this->extractXML($crawler, "//etym/mentioned");
        $this->etymManager->getAndAdd($entry, $etymoSrc, $mentioned);

        // SENSE
        $senses = $crawler->filterXPath('//sense')->each(function (Crawler $node) use ($crawler) {
            return $node;
        });
        $this->senseManager->getAndAdd($entry, $senses);

        return $entry;
    }


    public function entities2zip($onlyValidated)
    {
        $fileSystem = new Filesystem();

        $entries = ($onlyValidated) 
            ? $this->em->getRepository(Entry::class)->findByValid(1)
            : $this->em->getRepository(Entry::class)->findAll();

        $root = $this->params->get("kernel.project_dir") . "/public/xml/";
        $uniqDir = "export-".uniqid('', true);
        $zipName = $uniqDir .'.zip';
        $fullPath = $this->params->get("kernel.project_dir") . "/public/xml/".$uniqDir."/";
        foreach ($entries as $entry) {
            $this->entities2xml($entry, $fullPath);
        }
        $zip = new \ZipArchive();
        $ret = $zip->open($root . $zipName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        if ($ret !== true) {
            printf("A échoué avec le code d'erreur %d", $ret);
        } else {
            $options = ['add_path' => $fullPath, 'remove_all_path' => true];
            $zip->addGlob($fullPath . '/*.{xml}', GLOB_BRACE, $options);
            $zip->close();

            $fileSystem->remove($fullPath);
        }
        return $root . $zipName;
    }

    public function entitiespdf2zip()
    {
        $fileSystem = new Filesystem();

        $entries = $this->em->getRepository(Entry::class)->findByValid(1);

        $root = $this->params->get("kernel.project_dir") . "/public/pdf/";
        $uniqDir = "export-".uniqid('', true);
        $zipName = $uniqDir .'.zip';
        $fullPath = $this->params->get("kernel.project_dir") . "/public/pdf/".$uniqDir."/";
        foreach ($entries as $entry) {
            $this->entity2pdf($entry, $fullPath);
        }
        $zip = new \ZipArchive();
        $ret = $zip->open($root . $zipName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        if ($ret !== true) {
            printf("A échoué avec le code d'erreur %d", $ret);
        } else {
            $options = ['add_path' => $fullPath, 'remove_all_path' => true];
            $zip->addGlob($fullPath . '/*.{pdf}', GLOB_BRACE, $options);
            $zip->close();

            $fileSystem->remove($fullPath);
        }
        return $root . $zipName;
    }

    public function entity2pdf(Entry $entry, $path = null)
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($pdfOptions);
       
        $html = $this->twig->render('entry/display-pdf.html.twig', [
            'entry' => $entry
        ]);
      
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $output = $dompdf->output();

        $filename = $entry->getCleanLemma(). ".pdf";
        $fullPath = $path . $filename;
        $fileSystem = new Filesystem();
        $fileSystem->dumpFile($fullPath, $output);

        return $fullPath;
    }


    public function entities2xml(Entry $entry, $path = null)
    {
        $xmlContent = $this->twig->render('entry/export.html.twig', ['entry' => $entry]);

        $xmlContent = \tidy_repair_string($xmlContent, ['input-xml'=> 1, 'output-xml'=> 1, 'indent' => 1, 'wrap' => 0, 'hide-comments' => 1]);

        $path = (!$path) ? $this->params->get("kernel.project_dir") . "/public/xml/" : $path;
        $filename = $entry->getCleanLemma(). ".xml";
        $fullPath = $path . $filename;
        $fileSystem = new Filesystem();
        $fileSystem->dumpFile($fullPath, $xmlContent);

        return $fullPath;
    }

    public function extractXML($crawler, $xpath)
    {
        return $crawler->filterXPath($xpath)->text("");
    }

    public function getInitial($orth)
    {
        return $this->removeDiacritic(mb_substr(trim($orth), 0, 1));
    }

    public function removeDiacritic($str)
    {
        $str = \Transliterator::create('NFD; [:Nonspacing Mark:] Remove; NFC')->transliterate($str);

        return strtoupper($str);
    }

    public function searchEntry($stringSearch)
    {
        $stringSearch = mb_strtoupper($stringSearch);
        $exactMatches = [];
        $partialMatches = [];
        $exactMatch = $this->em->getRepository(Entry::class)->findOneBy(["lemma" => $stringSearch, "valid" => true]);
        if ($exactMatch) {
            $exactMatches[] = $exactMatch;
        }
        $stringCleaned = $this->removeDiacritic($stringSearch);
        $normalizeMatches = $this->em->getRepository(Entry::class)->findBy(["cleanLemma" => $stringCleaned, "valid" => true]);
        foreach ($normalizeMatches as $normalizeMatch) {
            if (!in_array($normalizeMatch, $exactMatches)) {
                $exactMatches[] = $normalizeMatch;
            }
        }
        $resultPartialMatches = $this->em->getRepository(Entry::class)->findByPartialLemma($stringSearch, $stringCleaned);
        foreach ($resultPartialMatches as $partialMatch) {
            if (!in_array($partialMatch, $exactMatches)) {
                $partialMatches[] = $partialMatch;
            }
        }

        return ["exactMatch" => $exactMatches, "partialMatch" => $partialMatches];
    }


    public function advancedSearchEntry($data)
    {
        $results = $this->em->getRepository(Entry::class)->findByAdvancedSearch($data);

        return $results;
    }
}
