<?php

namespace App\Manager;

use App\Entity\Entry;
use App\Entity\Gram;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;

class GramManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    public function getAndAdd(Entry $entry, $grams)
    {
        foreach ($entry->getGrams() as $gram) {
            $entry->removeGram($gram);
        }

        foreach ($grams as $g) {
            $text = $g["text"];
            $type = $g["type"];
            if (!$gram= $this->em->getRepository(Gram::class)->findOneBy(["text" => $text, "type" => $type])) {
                $gram = new Gram;
                $gram->setType($type);
                $gram->setText($text);
            }
            $entry->addGram($gram);
        }

        return $entry;
    }
}
