<?php

namespace App\Manager;

use App\Entity\Entry;
use App\Entity\Log;
use App\Entity\Sense;
use App\Entity\User;
use App\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\DomCrawler\Crawler;

class LogManager
{
    private $em;
    private $um;

    public function __construct(EntityManagerInterface $em, UserManager $um)
    {
        $this->em = $em;
        $this->um = $um;
    }

    public function createLog($object, $comment)
    {
        $date = new \DateTime();
        $user = $this->um->getCurrentUser();
        switch (ClassUtils::getClass($object)) {
          case 'App\Entity\Sense':
            $entry = $object->getEntry();
          break;

          case 'App\Entity\Entry':
            $entry = $object;
          break;
        }

        $log = new Log;
        $log->setUser($user);
        $log->setDate($date);
        $log->setEntry($entry);
        $log->setComment($comment);
        $entry->setDateEdition($date);
        $this->em->persist($entry);
        $this->em->persist($log);
        $this->em->flush();
        return $log;
    }
}
