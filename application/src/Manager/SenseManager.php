<?php

namespace App\Manager;

use App\Entity\Bibl;
use App\Entity\Cit;
use App\Entity\Domain;
use App\Entity\Entry;
use App\Entity\Note;
use App\Entity\Sense;
use App\Entity\Xr;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SenseManager
{
    private $em;
    private $params;

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $params)
    {
        $this->em = $em;
        $this->params = $params;
    }


    public function handleImage(Sense $sense, UploadedFile $file = null)
    {
        if ($file) {
            $this->deleteImage($sense);
            $fileName = $sense->getEntry()->getCleanLemma()."-".uniqid().".".$file->guessExtension();
            $filePath = $this->params->get("kernel.project_dir") . "/public/data/images/";
            $file->move($filePath, $fileName);
            $sense->setImgPath($fileName);
        }

        return;
    }

    public function deleteImage(Sense $sense)
    {
        $filePath = $this->params->get("kernel.project_dir") . "/public/data/images/";

        if ($sense->getImgPath()) {
            unlink($filePath.$sense->getImgPath());
            $sense->setImgPath(null);
        }

        return;
    }

    public function getAndAdd(Entry $entry, $senseNodes)
    {
        foreach ($entry->getSenses() as $sense) {
            $entry->removeSense($sense);
            $this->em->remove($sense);
        }

        foreach ($senseNodes as $senseNode) {
            $sense = new Sense;

            // DEF
            $def = $senseNode->filterXPath("//def")->html("");
            $sense->setDef($def);

            // USG
            $usg = $senseNode->filterXPath("//usg")->text("");
            $sense->setUsg($usg);

            // NOTES
            $notes = $senseNode->filterXPath('//note')->each(function (Crawler $node) {
                return $node;
            });
            $this->handleNotes($sense, $notes);

            // DOMAINS
            $domMedValue = $senseNode->attr("med") ? $senseNode->attr("med"): null;
            $domModValue = $senseNode->attr("mod") ? $senseNode->attr("mod"): null;
            $this->handleDomain($sense, $domMedValue, $domModValue);

            // CITS
            $citNode = $senseNode->filterXPath('//cit');
            $this->handleCit($sense, $citNode);

            $entry->addSense($sense);
        }
        return $entry;
    }

    public function handleDomain(Sense $sense, $medvalue, $modvalue)
    {
        if (!$domain = $this->em->getRepository(Domain::class)->findOneBy(["medValueShort" => $medvalue, "modValueShort" => $modvalue])) {
            $domain = new Domain;
            $domain->setMedValue("");
            $domain->setModValue("");
            $domain->setMedValueShort($medvalue);
            $domain->setModValueShort($modvalue);

            $this->em->persist($domain);
            $this->em->flush();
        }
        $sense->setDomain($domain);

        return $domain;
    }

    public function handleCit(Sense $sense, $citNode)
    {
        $quotes = $citNode->filterXPath('//quote')->each(function (Crawler $node, $i) {
            return $node;
        });
        $bibls = $citNode->filterXPath('//bibl')->each(function (Crawler $node, $i) {
            return $node;
        });
        $nums = $citNode->filterXPath('//num')->each(function (Crawler $node, $i) {
            return $node;
        });

        $i = 0;
        foreach ($bibls as $b) {
            $cit = new Cit;
            if (isset($quotes[$i])) {
                $quote = $quotes[$i]->html("");
                $num = (array_key_exists($i, $nums)) ? intval($nums[$i]->html(0)) : 0;
                $text = $b->html("");
                $author = $b->filterXPath('//author')->text("");
                $cit->setQuote($quote);
                $cit->setBiblText($text);
                $cit->setChronologicalIndex($num);
                $i++;
                $sense->addCit($cit);
            }
        }
    }

    public function handleNotes(Sense $sense, $notes)
    {
        foreach ($notes as $n) {
            $note = new Note;
            $type = $n->attr("id") ? $n->attr("id"): null;
            $note->setType($type);

            $xrs = $n->filterXPath("//xr")->each(function (Crawler $node) {
                $ref = $node->filterXPath("//ref")->text("");
                $gloss = $node->filterXPath("//gloss")->html("");
                $typeXR = $node->attr("type") ? $node->attr("type"): null;
                return ["type" => $typeXR, "ref" => $ref, "gloss" => $gloss];
            });
            $this->handleXrs($note, $xrs, false);

            $sense->addNote($note);
        }
    }

    public function handleXrs($entity, $xrs, $delete)
    {
        if ($delete) {
            foreach ($entity->getXrs() as $xr) {
                $entity->removeXr($xr);
                $this->em->remove($xr);
            }
        }
        foreach ($xrs as $x) {
            $xr = new Xr;
            $xr->setGloss($x["gloss"]);
            $xr->setRef($x["ref"]);
            $xr->setType($x["type"]);

            $entity->addXr($xr);
        }
    }
}
