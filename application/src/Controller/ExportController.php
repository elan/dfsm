<?php

namespace App\Controller;

use App\Entity\Entry;
use App\Manager\EntryManager;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class ExportController extends AbstractController
{
    private $entryManager;
    private $em;

    public function __construct(EntryManager $entryManager, EntityManagerInterface $em)
    {
        $this->entryManager = $entryManager;
        $this->em = $em;
    }

    /**
     * @Route("/entry/pdf/{id}", name="entry_pdf")
     */
    public function exportPdf(Entry $entry)
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($pdfOptions);
        $html = $this->renderView('entry/display-pdf.html.twig', [
            'entry' => $entry
        ]);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($entry->getCleanLemma(), [
            "Attachment" => true
        ]);
    }

    /**
     * @Route("admin/export-pdf", name="admin_export_pdf")
     */
    public function entitiespdf2zip()
    {   
        $zipName = $this->entryManager->entitiespdf2zip();

        $response = new BinaryFileResponse($zipName);
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            "export-dfsm".date("Y-m-d_H:i").".zip"
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->deleteFileAfterSend(true);
        
        return $response;
    }

    /**
     * @Route("/entry/xml/{id}", name="entry_xml")
     */
    public function exportXml(Entry $entry)
    {
        $filename = $this->entryManager->entities2xml($entry);
        $this->addFlash('success', "export OK");

        $response = new BinaryFileResponse($filename);
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $entry->getCleanLemma().".xml"
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->deleteFileAfterSend(true);
        
        return $response;
    }

    /**
     * @Route("/admin/export-zip/{onlyValidated}", name="admin_export_zip")
     */
    public function exportZip($onlyValidated)
    {
        $zipName = $this->entryManager->entities2zip($onlyValidated);

        $response = new BinaryFileResponse($zipName);
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            "export-dfsm".date("Y-m-d_H:i").".zip"
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->deleteFileAfterSend(true);
        
        return $response;
    }
}
