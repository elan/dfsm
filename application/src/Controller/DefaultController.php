<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Entry;

class DefaultController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        // $this->addFlash('notice', 'Toast flashmessages');

        $nbValidEntries = count($this->em->getRepository(Entry::class)->findBy(["valid" => true]));

        return $this->render('default/index.html.twig', 
        [
            "nbValidEntries" => $nbValidEntries  
        ]);
    }

    /**
     * @Route("/mention-legales", name="legal_notices")
     */
    public function legalNotices()
    {
        return $this->render('default/legal-notices.html.twig');
    }
}
