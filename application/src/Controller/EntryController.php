<?php

namespace App\Controller;

use App\Entity\Domain;
use App\Entity\Entry;
use App\Entity\Xr;
use App\Form\AdvancedSearchEntryType;
use App\Form\SearchEntryType;
use App\Manager\EntryManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EntryController extends AbstractController
{
    private $em;
    private $entryManager;

    public function __construct(EntityManagerInterface $em, EntryManager $entryManager)
    {
        $this->em = $em;
        $this->entryManager = $entryManager;
    }

    /**
     * @Route("/admin/last-edited-entries", name="admin_list_last_edited_entries")
     */
    public function lastEdited()
    {
        $entries = $this->em->getRepository(Entry::class)->findBy([], ["dateEdition" => "DESC"], 100);

        return $this->render('entry/list.html.twig', [
          "entries" => $entries,
          "from" => "last_edited_entries",
          "value" => null
        ]);
    }


    /**
     * @Route("/admin/debug", name="admin_debug")
     */
    public function debug()
    { 
        $badXrsforNotes = $this->em->getRepository(Xr::class)->findBadOnesforNotes();
        $badXrsforEntries = $this->em->getRepository(Xr::class)->findBadOnesforEntries();
        $xrValuesForNote = $this->em->getRepository(Xr::class)->findDistinctforNotes();
        $xrValuesForEntry = $this->em->getRepository(Xr::class)->findDistinctforEntries();

        return $this->render('admin/debug.html.twig', [
          "xrsNote" => $badXrsforNotes,
          "xrsEntry" => $badXrsforEntries,
          "xrValuesNote" => $xrValuesForNote,
          "xrValuesEntry" => $xrValuesForEntry
        ]);
    }


    /**
     * @Route("/admin/last-created-entries", name="admin_list_last_created_entries")
     */
    public function lastCreated()
    {
        $entries = $this->em->getRepository(Entry::class)->findBy([], ["dateCreation" => "DESC"], 100);

        return $this->render('entry/list.html.twig', [
          "entries" => $entries,
          "from" => "last_created_entries",
          "value" => null
        ]);
    }

    /**
     * @Route("/admin/list-invalid", name="admin_list_invalid")
     */
    public function invalid()
    {
        $entries = $this->em->getRepository(Entry::class)->findByValid(0);

        return $this->render('entry/list.html.twig', [
          "entries" => $entries,
          "from" => "invalid_entries",
          "value" => null
        ]);
    }

    /**
     * @Route("/entry/domain/med/{med}", name="entry_domain_med")
     */
    public function listByDomainMed($med)
    {
        $entries = $this->em->getRepository(Entry::class)->findByDomainMed($med);

        return $this->render('entry/list.html.twig', [
          "from" => "domain",
          "value" => $med,
          "entries" => $entries
        ]);
    }

    /**
     * @Route("/entry/search/start", name="entry_start_with", options={"expose"=true})
     */
    public function searchByStart(Request $request)
    {
        $start = $request->get("start");
        $cleanStart = $this->entryManager->removeDiacritic($start);
        $entries = $this->em->getRepository(Entry::class)->findByStartWith($start, $cleanStart);

        return $this->render('entry/partial/start-with.html.twig', ['entries' => $entries]);
    }

    /**
     * @Route("/entry/domain/{id}", name="entry_domain")
     */
    public function listByDomain(Domain $domain)
    {
        $entries = $this->em->getRepository(Entry::class)->findByDomain($domain);

        return $this->render('entry/list.html.twig', [
          "from" => "domain",
          "value" => $domain->getModValueShort(),
          "entries" => $entries
        ]);
    }

    /**
     * @Route("/entry/display/{id}", name="entry_display")
     * @Route("/entry/{lemma}", name="entry_display_lemma")
     */
    public function display(Entry $entry)
    {
        if ($entry->getValid() || $this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->render('entry/display.html.twig', ["entry" => $entry]);
        }

        $this->addFlash('notice', 'Entrée en cours de validation.');

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/entry/bylemma/{lemma}", name="entry_bylemma")
     */
    public function refLemma(Entry $entry = null)
    {
        return $this->redirectToRoute("entry_display", ["id" => $entry->getId()]);
    }

    /**
     * @Route("/entry/search/simple", name="entry_search")
     */
    public function search(Request $request)
    {
        $form = $this->createForm(SearchEntryType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $search = $form->get('search')->getData();
            $results = $this->entryManager->searchEntry($search);

            return $this->render('entry/results.html.twig', [
              "search"  => $search,
              "results" => $results,
              "form" => $form->createView()
            ]);
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/entry/search/advanced", name="entry_advanced_search")
     */
    public function advancedSearch(Request $request)
    {
        $form = $this->createForm(AdvancedSearchEntryType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $entries = $this->entryManager->advancedSearchEntry($data);

            return $this->render('entry/list.html.twig', [
              "from" => "advanced-search",
              "value" => "",
              "entries" => $entries
            ]);
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/entry/initial/{initial}", name="entry_initial")
     */
    public function listByInitial($initial)
    {
        $entries = $this->em->getRepository(Entry::class)->findBy(["initial" => $initial, "valid" => true], ["lemma" => "ASC"]);

        return $this->render('entry/list.html.twig', [
          "from" => "initial",
          "value" => $initial,
          "entries" => $entries
        ]);
    }
}
