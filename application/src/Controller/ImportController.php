<?php

namespace App\Controller;

use App\Form\XmlType;
use App\Form\ZipType;
use App\Manager\EntryManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin_")
 */
class ImportController extends AbstractController
{
    private $entryManager;

    public function __construct(EntryManager $entryManager)
    {
        $this->entryManager = $entryManager;
    }

    /**
     * @Route("/zip", name="import_zip")
     */
    public function importZip(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        $form = $this->createForm(ZipType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $zipFile = $form->get('zip')->getData();
            $autoValid = $form->get('auto_valid')->getData();
            $zipName = $zipFile->getClientOriginalName();
            $uploadPath = "entries".DIRECTORY_SEPARATOR;
            $zipFile->move($uploadPath, $zipName);
            $zip = new \ZipArchive;

            if ($zip->open($uploadPath . DIRECTORY_SEPARATOR . $zipName)) {
                for ($i = 0; $i < $zip->numFiles; $i++) {
                    $name = $zip->getNameIndex($i);
                    if (mb_substr($name, -4) == ".xml") {
                        $content = $zip->getFromIndex($i);
                        $this->entryManager->importEntry($content, $autoValid);
                    }
                }

                $zip->close();
                $filesystem = new Filesystem();
                $filesystem->remove($uploadPath.$zipName);
            }
            return $this->redirectToRoute('homepage');
        } else {
            return $this->render('admin/import-zip.html.twig', [
              'form' => $form->createView()
            ]);
        }
    }

    /**
     * @Route("/xml", name="import_xml")
     */
    public function importXml(Request $request)
    {
        $form = $this->createForm(XmlType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $autoValid = $form->has('auto_valid') ? $form->get('auto_valid')->getData() : false;
            $xmlFile = $form->get('xml')->getData();
            $content = file_get_contents($xmlFile->getPathname());
            $this->entryManager->importEntry($content, $autoValid);

            return $this->redirectToRoute('homepage');
        } else {
            return $this->render('admin/import-xml.html.twig', [
              'form' => $form->createView()
            ]);
        }
    }
}
