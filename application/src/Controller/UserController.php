<?php

namespace App\Controller;

use App\Entity\User;
use App\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/super-admin", name="admin_")
 */
class UserController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/users", name="list_users")
     */
    public function index()
    {
        $users = $this->em->getRepository(User::class)->findAll();

        return $this->render('admin/users.html.twig', [
          "users" => $users
        ]);
    }


    /**
     * @Route("/user/role/{id}/{role}", name="set_role")
     */
    public function setRole(User $user, $role, UserManager $um)
    {
        $um->setRole($user, [$role]);
        $this->addFlash('success', "switch admin done");

        return $this->redirectToRoute('admin_list_users');
    }

    /**
     * @Route("/user/{id}", name="delete_user")
     */
    public function deleteUser(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $this->addFlash('success', "Utilisateur supprimé");

        return $this->redirectToRoute('admin_list_users');
    }

    /**
     * @Route("/users/export", name="export_users")
     */
    public function exportUsers()
    {
        $encoders = [new CsvEncoder(";")];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        $fileSystem = new Filesystem();
        $root = $this->getParameter("kernel.project_dir") . "/public/";
        $file = "export/" . uniqid() . ".csv";
        $csvContent = $serializer->serialize($users, 'csv');
        $fileSystem->appendToFile($root.$file, $csvContent);
        $this->addFlash('success', "export OK");

        return $this->render('admin/users.html.twig', [
         "users" => $users,
         "file" => $file
        ]);
    }
}
