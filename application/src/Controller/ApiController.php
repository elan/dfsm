<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\HeaderUtils;

use App\Entity\Entry;


/**
 * @Route("/api/", name="api_")
 */
class ApiController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("entries-csv", name="entries_csv")
     */
    public function entriesCsv()
    {
        $entries = $this->em->getRepository(Entry::class)->findByValid(true, ["lemma" => "ASC"]);

        $fileContent = "";
        foreach ($entries as $entry) {
            $fileContent .= $entry->getLemma();
            $fileContent .= "\t";
            $fileContent .= $entry->getId();
            $fileContent .= "\t";

            if ($entry->isVariante() === true) {
                $xr = $entry->getXrs()[0];
                if ($xr->getType() == "V.") {
                    $fileContent .= $xr->getRef();
                }
            }

            $fileContent .= "\n";
        }


        $response = new Response($fileContent);
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            "dfsm-entries-" . date("Ymd") . ".csv"
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }


    /**
     * @Route("entries-json", name="entries_json")
     */
    public function entriesJson()
    {
        $entries = $this->em->getRepository(Entry::class)->findByValid(true, ["lemma" => "ASC"]);
        $arrayOut = [];

        foreach ($entries as $entry) {
            if (!$entry->isVariante()) {
                $out = [];

                $out["lemma"] = $entry->getLemma();
                $out["initial"] = $entry->getInitial();
                $out["grams"] = [];
                foreach ($entry->getGrams() as $gram) {
                    $out["grams"][] = $gram->getType() . " - " .  $gram->getText();
                }

                $out["domain-med"] = [];
                foreach ($entry->getSenses() as $sense) {
                    $out["domain-med"][] = $sense->getDomain()->getMedValueShort();
                }

                $out["domain-mod"] = [];
                foreach ($entry->getSenses() as $sense) {
                    $out["domain-mod"][] = $sense->getDomain()->getModValueShort();
                }

                $out["def"] = [];
                foreach ($entry->getSenses() as $sense) {
                    $out["def"][] = $sense->getDef();
                }

                $arrayOut[] = $out;
            }
        }

        return new JsonResponse($arrayOut);
    }
}
