<?php

namespace App\Controller;

use App\Entity\Log;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin_")
 */
class LogController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/last-logs", name="last_logs")
     */
    public function lastLogs()
    {
        $logs = $this->em->getRepository(Log::class)->findBy([], ["date" => "DESC"], 100);

        return $this->render('log/list.html.twig', [
          "logs" => $logs,
          "from" => "last_log",
          "value" => null
        ]);
    }
}
