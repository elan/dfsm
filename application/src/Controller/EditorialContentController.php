<?php

namespace App\Controller;

use App\Entity\EditorialContent;
use App\Manager\EditorialContentManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\EditorialContentType;

class EditorialContentController extends AbstractController
{
    private $em;
    private $editorialContentManager;

    public function __construct(EntityManagerInterface $em, EditorialContentManager $editorialContentManager)
    {
        $this->em = $em;
        $this->editorialContentManager = $editorialContentManager;
    }

    /**
     * @Route("/edito/{name}", name="edito_display")
     */
    public function display($name)
    {
        $editorialContent = $this->em->getRepository(EditorialContent::class)->findOneByName($name);

        return $this->render('edito/display.html.twig', [
          "content" => $editorialContent,
        ]);
    }


    /**
     * @Route("/admin/edito/edit/{id}", name="edito_edit")
     */
    public function edit(EditorialContent $editorialContent, Request $request)
    {
        $form = $this->createForm(EditorialContentType::class, $editorialContent);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($editorialContent);
            $this->em->flush();

            $this->addFlash('notice', 'Contenu éditorial sauvegardée');

            return $this->redirectToRoute("homepage");
        }

        return $this->render('edito/edit.html.twig', [
          "content" => $editorialContent,
          "form" => $form->createView(),
        ]);
    }
}
