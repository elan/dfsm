<?php

namespace App\Controller;

use App\Entity\Cit;
use App\Entity\Domain;
use App\Entity\Entry;
use App\Entity\Gram;
use App\Entity\Log;
use App\Entity\Note;
use App\Entity\Sense;
use App\Entity\Xr;
use App\Form\CitType;
use App\Form\DomainType;
use App\Form\EntryType;
use App\Form\GramType;
use App\Form\NoteType;
use App\Form\SenseType;
use App\Form\XrFullType;
use App\Manager\EntryManager;
use App\Manager\LogManager;
use App\Manager\SenseManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin_")
 */
class EditorController extends AbstractController
{
    private $em;
    private $entryManager;
    private $senseManager;
    private $lm;

    public function __construct(EntityManagerInterface $em, EntryManager $entryManager, SenseManager $senseManager, LogManager $lm)
    {
        $this->em = $em;
        $this->lm = $lm;
        $this->entryManager = $entryManager;
        $this->senseManager = $senseManager;
    }

    /**
     * @Route("/sense/{id}/image/delete/", name="delete_image", options={"expose"=true})
     */
    public function deleteImage(Sense $sense)
    {
        $this->senseManager->deleteImage($sense);
        $entry = $sense->getEntry();
        $entry->setValid(false);

        $this->em->persist($entry);
        $this->em->persist($sense);
        $this->em->flush();

        $this->lm->createLog($sense, "Suppression image");

        return new JsonResponse();
    }

    /**
     * @Route("/entry/{entryId}/xr/edit/{id}", name="xr_edit", defaults={"id"=null})
     * @ParamConverter("entry", options={"id" = "entryId"})
     */
    public function editXr(Xr $xr = null, Entry $entry, Request $request)
    {
        if (!$xr) {
            $xr = new Xr;
            $xr->setEntry($entry);
        }

        $form = $this->createForm(XrFullType::class, $xr);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entry->setValid(false);

            $this->em->persist($entry);
            $this->em->persist($xr);
            $this->em->flush();

            $this->lm->createLog($entry, "Edition/création référence");

            $this->addFlash('notice', 'Référence sauvegardée');

            return $this->redirectToRoute("admin_entry_edit", ["id" => $entry->getId()]);
        }

        return $this->render('xr/edit.html.twig', [
          "xr" => $xr,
          "form" => $form->createView(),
        ]);
    }


    /**
     * @Route("/sense/{senseId}/citation/edit/{id}", name="cit_edit", defaults={"id"=null})
     * @ParamConverter("sense", options={"id" = "senseId"})
     */
    public function editCit(Cit $cit = null, Sense $sense, Request $request)
    {
        if (!$cit) {
            $cit = new Cit;
            $cit->setSense($sense);
        }

        $form = $this->createForm(CitType::class, $cit);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entry = $sense->getEntry();
            $entry->setValid(false);

            $this->em->persist($entry);
            $this->em->persist($cit);
            $this->em->flush();

            $this->lm->createLog($sense, "Edition/création citation");

            $this->addFlash('notice', 'Citation sauvegardée');

            return $this->redirectToRoute("admin_sense_edit", ["entryId" => $sense->getEntry()->getId(),"id" => $sense->getId()]);
        }

        return $this->render('cit/edit.html.twig', [
          "cit" => $cit,
          "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/sense/{senseId}/note/edit/{id}", name="note_edit", defaults={"id"=null})
     * @ParamConverter("sense", options={"id" = "senseId"})
     */
    public function editNote(Note $note = null, Sense $sense, Request $request)
    {
        if (!$note) {
            $note = new Note;
            $note->setSense($sense);
        }

        $originalXrs = new ArrayCollection();
        foreach ($note->getXrs() as $xr) {
            $originalXrs->add($xr);
        }

        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($originalXrs as $originalXr) {
                if ($note->getXrs()->contains($originalXr) === false) {
                    $this->em->remove($originalXr);
                }
            }

            foreach ($note->getXrs() as $newXr) {
                $newXr->setNote($note);
            }
            $entry = $sense->getEntry();
            $entry->setValid(false);

            $this->em->persist($entry);

            $this->em->persist($note);
            $this->em->flush();

            $this->lm->createLog($sense, "Edition/création note");

            $this->addFlash('notice', 'Note sauvegardée');

            return $this->redirectToRoute("admin_sense_edit", ["entryId" => $sense->getEntry()->getId(),"id" => $sense->getId()]);
        }

        return $this->render('note/edit.html.twig', [
          "note" => $note,
          "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/entry/{entryId}/sense/edit/{id}", name="sense_edit", defaults={"id"=null})
     * @ParamConverter("entry", options={"id" = "entryId"})
     */
    public function editSense(Sense $sense = null, Entry $entry, Request $request)
    {
        if (!$sense) {
            $sense = new Sense;
            $sense->setEntry($entry);
        }

        $form = $this->createForm(SenseType::class, $sense);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->get('image')->getData();
            $this->senseManager->handleImage($sense, $image);
            $entry->setValid(false);

            $this->em->persist($entry);
            $this->em->persist($sense);
            $this->em->flush();

            $this->lm->createLog($entry, "Edition/création sens");

            $this->addFlash('notice', 'Sens sauvegardée');

            return $this->redirectToRoute("admin_entry_edit", ["id" => $entry->getId()]);
        }

        return $this->render('sense/edit.html.twig', [
          "sense" => $sense,
          "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/gram/delete/{id}", name="delete_gram")
     */
    public function deleteGram(Gram $gram)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        if (count($this->em->getRepository(Entry::class)->findByGramGrp($gram)) < 1) {
            $this->em->remove($gram);
            $this->em->flush();

            $this->addFlash('notice', 'Gram supprimé');
        }

        $this->addFlash('warning', 'Des entrées sont encore liées.');


        return $this->redirectToRoute("admin_gram_list");
    }

    /**
     * @Route("/domain/delete/{id}", name="delete_domain")
     */
    public function deleteDomain(Domain $domain)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        $this->em->remove($domain);
        $this->em->flush();

        $this->addFlash('notice', 'Domaine supprimé');

        return $this->redirectToRoute("admin_domain_list");
    }

    /**
     * @Route("/xr/delete/{id}", name="delete_xr")
     */
    public function deleteXr(Xr $xr)
    {
        $entry = $xr->getEntry();
        $entry->setValid(false);

        $this->em->remove($xr);
        $this->em->persist($entry);
        $this->em->flush();

        $this->lm->createLog($entry, "Suppression référence");

        $this->addFlash('notice', 'Référence supprimée');
        return $this->redirectToRoute("admin_entry_edit", ["id" => $entry->getId()]);
    }

    /**
     * @Route("/cit/delete/{id}", name="delete_cit")
     */
    public function deleteCit(Cit $cit)
    {
        $sense = $cit->getSense();
        $entry = $sense->getEntry();
        $entry->setValid(false);

        $this->em->persist($entry);
        $this->em->remove($cit);
        $this->em->flush();

        $this->lm->createLog($entry, "Suppression citation");

        $this->addFlash('notice', 'Citation supprimée');

        return $this->redirectToRoute("admin_sense_edit", ["entryId" => $entry->getId(), "id" => $sense->getId()]);
    }

    /**
     * @Route("/note/delete/{id}", name="delete_note")
     */
    public function deleteNote(Note $note)
    {
        $sense = $note->getSense();
        $entry = $sense->getEntry();
        $entry->setValid(false);

        $this->em->persist($entry);
        $this->em->remove($note);
        $this->em->flush();

        $this->lm->createLog($entry, "Suppression note");

        $this->addFlash('notice', 'Note supprimée');

        return $this->redirectToRoute("admin_sense_edit", ["entryId" => $entry->getId(), "id" => $sense->getId()]);
    }

    /**
     * @Route("/sense/delete/{id}", name="delete_sense")
     */
    public function deleteSense(Sense $sense)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        $entry = $sense->getEntry();
        $entry->setValid(false);

        $this->em->persist($entry);
        $this->em->remove($sense);
        $this->em->flush();

        $this->lm->createLog($entry, "Suppression sens");

        $this->addFlash('notice', 'Sens supprimé');

        return $this->redirectToRoute("admin_entry_edit", ["id" => $entry->getId()]);
    }


    /**
     * @Route("/gram/edit/{id}", name="edit_gram", defaults={"id"=null})
     */
    public function editGram(Gram $gram = null, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        if (!$gram) {
            $gram = new Gram;
        }

        $form = $this->createForm(GramType::class, $gram);


        $entries = (!$gram->getId()) ? null :$this->em->getRepository(Entry::class)->findByGramGrp($gram);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($gram);
            $this->em->flush();

            $this->addFlash('notice', 'Gram sauvegardé');

            return $this->redirectToRoute("admin_gram_list");
        }


        return $this->render('gram/edit.html.twig', [
          "gram" => $gram,
          "entries" => $entries,
          "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/domain/edit/{id}", name="edit_domain", defaults={"id"=null})
     */
    public function editDomain(Domain $domain = null, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        if (!$domain) {
            $domain = new Domain;
        }

        $form = $this->createForm(DomainType::class, $domain);

        $senses = (!$domain->getId()) ? null :$this->em->getRepository(Sense::class)->findByDomainSorted($domain);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($domain);
            $this->em->flush();

            $this->addFlash('notice', 'Domaine sauvegardé');

            return $this->redirectToRoute("admin_domain_list");
        }

        return $this->render('domain/edit.html.twig', [
          "domain" => $domain,
          "senses" => $senses,
          "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/gram/list", name="gram_list")
     */
    public function listGrams()
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        $grams = $this->em->getRepository(Gram::class)->findBy([], ["type" => "ASC"]);

        return $this->render('gram/list.html.twig', [
          "grams" => $grams,
        ]);
    }

    /**
     * @Route("/domain/list", name="domain_list")
     */
    public function listDomains()
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        $domains = $this->em->getRepository(Domain::class)->findBy([], ["medValueShort" => "ASC"]);

        return $this->render('domain/list.html.twig', [
          "domains" => $domains,
        ]);
    }

    /**
     * @Route("/entry/edit/{id}", name="entry_edit", defaults={"id"=null})
     */
    public function editEntry(Entry $entry = null, Request $request)
    {
        if (!$entry) {
            $entry = new Entry;
            $entry->setDateCreation(new \DateTime());
        }

        $form = $this->createForm(EntryType::class, $entry);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $lemma = mb_strtoupper($entry->getLemma());
            $initial = $this->entryManager->getInitial($lemma);
            $cleanLemma = $this->entryManager->removeDiacritic($lemma);
            $entry->setValid(false);
            $entry->setLemma($lemma);
            $entry->setInitial($initial);
            $entry->setCleanLemma($cleanLemma);
            $entry->setDateEdition(new \DateTime());
            $this->em->persist($entry);
            $this->em->flush();

            $this->lm->createLog($entry, "Edition/création entrée");

            $this->addFlash('notice', 'Entrée sauvegardée');

            return $this->redirectToRoute("admin_entry_edit", ["id" => $entry->getId()]);
        }

        return $this->render('entry/edit.html.twig', [
          "entry" => $entry,
          "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/entry/toggle-valid/{id}", name="toggle_valid")
     */
    public function toggleValid(Entry $entry)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        $entry->setValid(!$entry->getValid());
        $this->em->persist($entry);
        $this->em->flush();

        $this->addFlash('notice', 'entry_validity_changed');

        return $this->redirectToRoute('entry_display', ["id" => $entry->getId()]);
    }

    /**
     * @Route("/delete-entry/{id}", name="delete_entry")
     */
    public function deleteEntry(Entry $entry)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        $em = $this->getDoctrine()->getManager();

        $fileXml = $this->getParameter("kernel.project_dir") . "/public/xml/".$entry->getId().".xml";
        if (is_file($fileXml)) {
            unlink($fileXml);
        }

        $em->remove($entry);
        $em->flush();
        $this->addFlash('notice', 'Entrée supprimée');

        return $this->redirectToRoute('homepage');
    }
}
