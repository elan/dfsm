<?php

namespace App\Form;

use App\Entity\Domain;
use App\Form\XrType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('type', TextType::class, [
            'label' => "Type de note",
            'help' => "Correspond à l'attribut 'id' de la balise 'note'"
          ])

          ->add('xrs', CollectionType::class, [
            'label' => false,
            'entry_type' => XrType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true
          ])

          ->add('submit', SubmitType::class, array(
              'label' => 'save',
          ));
    }
}
