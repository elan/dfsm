<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchEntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('search', TextType::class, [
            'mapped' => false,
            'help' => "Si vous pressez la touche entrée ou cliquez sur 'Rechercher', la recherche sera lancée.",
            'label' => false,
          ])
          ->add('submit', SubmitType::class, array(
              'label' => 'search',
          ));
    }
}
