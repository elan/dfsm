<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class XrType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('type', TextType::class, [
            'required' => false,
            'label' => "Type de référence (var, syn, etc.)",
            'attr' => ['data-type' => 'struct'],
            'help' => "Correspond à l'attribut 'type' de 'xr'"
          ])
          ->add('ref', TextType::class, [
            'required' => false,
            'label' => "Référence",
            'attr' => ['data-type' => 'struct'],
            'help' => "Correspond au contenu de la balise 'ref'"
          ])
          ->add('gloss', TextType::class, [
            'required' => false,
            'label' => "Glose",
            'attr' => ['class'=>'tinymce', 'data-theme' => 'limited', 'data-type' => 'encyclo'],
            'help' => "Correspond au contenu de la balise 'gloss'"
          ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Xr',
        ));
    }
}
