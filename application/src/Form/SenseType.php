<?php

namespace App\Form;

use App\Entity\Domain;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class SenseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('def', TextType::class, [
            'label' => "Définition",
            'attr' => ['class'=>'tinymce', 'data-theme' => 'limited'],
            'help' => "Correspond à la balise 'def'",
          ])
          ->add('usg', TextType::class, [
            'label' => "Unité polylexicale",
            'required' => false,
            'help' => "Correspond à la balise 'usg'",
          ])
          ->add('domain', EntityType::class, [
            'class' => Domain::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('d')
                          ->addOrderBy('d.medValueShort', 'ASC')
                          ->addOrderBy('d.modValueShort', 'ASC');
            },
            'choice_label' => function ($domain) {
                $str = "";
                $str .=  ($domain->getMedValue()) ? $domain->getMedValue() : $domain->getMedValueShort();
                $str .= " - ";
                $str .=  ($domain->getModValue()) ? $domain->getModValue() : $domain->getModValueShort();
                return $str;
            },
            'expanded' => false,
            'multiple' => false,
            'label' => "Domaines",
            'help' => "Correspond aux attributs 'med' et 'mod' de 'sense'"
          ])
          ->add('image', FileType::class, [
                'label' => 'image',
                'mapped' => false,
                'required' => false,
                'attr' => [
                  'accept' => 'image/*',
                ],
                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '10024k',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid Image document',
                    ])
                ],
            ])
          ->add('submit', SubmitType::class, [
              'label' => 'save_sense',
          ]);
    }
}
