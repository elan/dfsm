<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EtymType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('src', TextType::class, [
            'label' => "src",
            'required' => false,
            'help' => "Correspond à la balise 'src' / Par exemple, \"FEW XV-1 307a :\". Si absent de FEW, copier 'FEW : ∅'"
          ])
          ->add('mentioned', TextType::class, [
            'label' => "Etymon",
            'required' => false,
            'help' => "Correspond à la balise 'mentioned' / Si absent de FEW, copier '?'",
          ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Etym',
        ));
    }
}
