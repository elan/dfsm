<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;



class XmlType extends AbstractType
{
    private $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
      $this->authorizationChecker=$authorizationChecker;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('xml', FileType::class, [
            'mapped' => false,
            'label' => 'file_xml',
            'attr' => ['accept' => 'application/xml'],
          ]);

          if($this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN')){
              $builder->add('auto_valid', CheckboxType::class, [
                  'mapped' => false,
                  'label' => 'auto_valid',
                  'required' => false,
                ]);
          }

          $builder->add('save', SubmitType::class,[
              'label' => 'Envoyer'
          ]);
    }
}
