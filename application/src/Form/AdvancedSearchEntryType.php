<?php

namespace App\Form;

use App\Entity\Domain;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvancedSearchEntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('orth', TextType::class, [
            'label' => "Lemme",
            'help' => "Correspond à la balise 'orth'.",
            'required' => false
          ])
          ->add('def', TextType::class, [
            'label' => "def",
            'help' => "Correspond à la balise 'def'.",
            'required' => false
          ])
          ->add('usg', TextType::class, [
            'label' => "Unité polylexicale",
            'help' => "Correspond à la balise 'usg'.",
            'required' => false
          ])
          ->add('domain', EntityType::class, [
            'class' => Domain::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('d')
                          ->addOrderBy('d.medValueShort', 'ASC')
                          ->addOrderBy('d.modValueShort', 'ASC');
            },
            'choice_label' => function ($domain) {
                $str = ($domain->getMedValue()) ? $domain->getMedValue() : $domain->getMedValueShort();
                $str .= " - ";
                $str .= ($domain->getModValue()) ? $domain->getModValue() : $domain->getModValueShort();
                return $str;
            },
            'expanded' => false,
            'multiple' => false,
            'label' => "Domaine",
            'help' => "Correspond aux attributs 'med' et 'mod' de 'sense'.",
            'required' => false
          ])
          ->add('etym', TextType::class, [
            'label' => "Étymon",
            'help' => "Correspond à la balise 'etym'.",
            'required' => false
          ])
          ->add('gloss', TextType::class, [
            'label' => "Glose",
            'help' => "Correspond à la balise 'gloss'.",
            'required' => false
          ])
          ->add('quote', TextType::class, [
            'label' => "Citation",
            'help' => "Correspond à la balise 'quote'.",
            'required' => false
          ])
          ->add('biblText', TextType::class, [
            'label' => "Référence bibliographique",
            'help' => "Correspond à la balise 'bibl'.",
            'required' => false
          ])
          ->add('submit', SubmitType::class, [
              'label' => 'search',
          ]);
    }
}
