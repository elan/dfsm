<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GramType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('type', TextType::class, [
            'label' => "type",
          ])

          ->add('text', TextType::class, [
            'label' => "text",
          ])

          ->add('submit', SubmitType::class, array(
              'label' => 'Sauvegarder',
          ));
    }
}
