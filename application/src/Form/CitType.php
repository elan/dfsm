<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('quote', TextType::class, [
            'label' => "Texte de la citation",
            'help' => "Correspond à la balise 'quote' précédant une balise 'bibl'"
          ])
          ->add('biblText', TextType::class, [
            'required' => false,
            'label' => "Référence bibliographique",
            'help' => "Correspond à la balise 'bibl'"
          ])
          ->add('chronologicalIndex', IntegerType::class, [
            'required' => true,
            'label' => "Index tri",
            'help' => "Aide à trier par ordre chronologique"
          ])
          ->add('submit', SubmitType::class, array(
              'label' => 'save',
          ));
    }
}
