<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class XrFullType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('type', TextType::class, [
            'label' => "Type de référence (var, syn, etc.)",
            'help' => "Correspond à l'attribut 'type' de 'xr'"
          ])
          ->add('ref', TextType::class, [
            'required' => false,
            'label' => "Référence",
            'help' => "Correspond au contenu de la balise 'ref'"
          ])
          ->add('gloss', TextType::class, [
            'required' => false,
            'label' => "Glose",
            'attr' => ['class'=>'tinymce', 'data-theme' => 'limited'],
            'help' => "Correspond au contenu de la balise 'gloss'"
          ])
          ->add('submit', SubmitType::class, array(
              'label' => 'save',
          ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Xr',
        ));
    }
}
