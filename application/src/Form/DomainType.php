<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DomainType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('medValue', TextType::class, [
            'label' => "domain_medValue",
          ])
          ->add('modValue', TextType::class, [
            'label' => "domain_modValue",
          ])
          ->add('medValueShort', TextType::class, [
            'label' => "domain_medValueShort",
          ])

          ->add('modValueShort', TextType::class, [
            'label' => "domain_modValueShort",
          ])
          ->add('submit', SubmitType::class, array(
              'label' => 'Sauvegarder',
          ));
    }
}
