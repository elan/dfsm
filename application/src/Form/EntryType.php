<?php

namespace App\Form;

use App\Form\EtymType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('lemma', TextType::class, [
            'label' => "Vedette",
            'help'  => "Correspond à la balise 'orth'"
          ])
          ->add('etym', EtymType::class, [
            'label' => "etym",
            'required' => false,
            'attr' => ["class" => 'pl-3']
          ])
          ->add('grams', EntityType::class, [
            'class' => 'App:Gram',
            'choice_label' => function ($gram) {
                return $gram->getType() . ' : ' . $gram->getText();
            },
            'label' => "grams",
            'help' => "Correspond aux balises 'gram' du 'gramgrp'",
            'expanded'  => false,
            'required'  => false,
            'multiple'  => true,
          ])
          ->add('submit', SubmitType::class, array(
              'label' => 'save',
          ));
    }
}
