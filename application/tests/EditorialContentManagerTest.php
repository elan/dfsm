<?php

namespace App\Test;

use PHPUnit\Framework\TestCase;
use App\Manager\EditorialContentManager;
use Doctrine\ORM\EntityManagerInterface;

class EditorialContentManagerTest extends TestCase
{
    // private $em;
    // private $class;
    //
    // public function __construct(EntityManagerInterface $em, $class)
    // {
    //     $parent::__construct();
    //     $this->em = $em;
    //     $this->class = new EditorialContentManager($em);
    // }
    //

    /**
   * @dataProvider provideNames()
   */
    public function testCreate($name)
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $manager = new EditorialContentManager($em);
        $editorialContent = $manager->create($name);

        $this->assertTrue(get_class($editorialContent) == 'App\Entity\EditorialContent');
    }

    public function provideNames()
    {
        return [
        ["nom 1"], ["nom 2"], ["nom 3"]
      ];
    }
}
