var $collectionHolder;
var $addXrButton = $('#addXr');

jQuery(document).ready(function() {
    // Get the ul that holds the collection of tags
    $collectionHolder = $('div.xrs');
    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find('div.xr-element').length);

    $collectionHolder.find('div.xr-element').each(function() {
        addXrDeleteLink($(this));
    });

    $addXrButton.on('click', function(e) {
        addXrForm($collectionHolder);
    });

    $("#note_type").on("change keyup paste", function(){
        hideUselessField();
    })

    hideUselessField();
});

function hideUselessField() {
    // depending on note type, hide some field 
    //  - ref and ref type when "encyclo"
    //  - glose when "struct"
    let noteType = document.querySelector("#note_type").value.toLowerCase()
    let inputsToHide = []
    let inputsToShow = []
    if (noteType.startsWith("encyclo")) {
        inputsToHide = document.querySelectorAll("[data-type='struct']") 
        inputsToShow = document.querySelectorAll("[data-type='encyclo']") 
    } else if (noteType.startsWith("struct")) { 
        inputsToHide = document.querySelectorAll("[data-type='encyclo']") 
        inputsToShow = document.querySelectorAll("[data-type='struct']") 
    } else {
        inputsToShow = document.querySelectorAll("[data-type='struct'],[data-type='encyclo']") 
    }

    inputsToHide.forEach(element => {
        element.parentNode.style.display = "none"
    });
    inputsToShow.forEach(element => {
        element.parentNode.style.display = "block"
    });
}

function addXrForm($collectionHolder) {
    var prototype = $collectionHolder.data('prototype');
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    newForm = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    $collectionHolder.append(newForm);

    addXrDeleteLink($collectionHolder.find("div.xr-element").last());
    hideUselessField();
}

function addXrDeleteLink($xrLi) {
    var $removeFormButton = $('<span class="btn btn-sm btn-danger pull-right"><i class="fa fa-trash" aria-hidden="true"></i></span>');

    $xrLi.find(".card .card-body").append($removeFormButton);

    $removeFormButton.on('click', function(e) {
        $xrLi.remove();
    });
}
