$(function() {
  $('#delete-sense-image').bind('click', function() {
    deleteImage($(this).data("sense"));
  });
})

function deleteImage(id) {
  let url = Routing.generate('admin_delete_image', {
    id: id
  })

  $.ajax({
    method: 'POST',
    url: url
  }).done((results) => {
    document.getElementById("sense-image").innerHTML = ""
    $('#delete-image-modal').modal('hide')
    Toastr.info("Image supprimée")
  });
}