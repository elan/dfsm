let input = document.getElementById("rechercheContrainte")

input.addEventListener("keyup", function() {
  filter(this.value)
}, false);

function filter(value) {
  let entries = document.getElementsByClassName("entry");
  let entriesArray = Array.from(entries)

  entriesArray.forEach(function(entry) {
    let lemma = entry.dataset.lemma
    let cleanLemma = entry.dataset.cleanlemma
    entry.style.display = (lemma.indexOf(value.toUpperCase()) > -1 || cleanLemma.indexOf(value.toUpperCase()) > -1) ? "block" : "none"
  });
}
