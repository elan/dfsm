$(function() {
  $('#search_entry_search').bind('input', function() {
    doSearch($(this).val());
  });

})

var delayTimer;

function doSearch(start) {
  if (start.length) {
    clearTimeout(delayTimer);
    delayTimer = setTimeout(function() {

      let url = Routing.generate('entry_start_with');

      $.ajax({
        method: 'POST',
        url: url,
        data: {
          'start': start
        }
      }).done((results) => {
        document.getElementById("results-temp").innerHTML = results
      })

    }, 500);
  } else {
    document.getElementById("results-temp").innerHTML = ""
  }
}